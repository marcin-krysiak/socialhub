/**
 * Created by elf on 11/09/15.
 */

var express = require('express'),
    router = express.Router(),
    db = require('monk')('localhost/dump'),
    addons = db.get('addons'),
    accounts = db.get('accounts'),
    users = db.get('users');

var sendEmail = function(addonId) {
    var account, username;

    accounts.find({}, function (err, data){
        if (!err) {
            account = data[0].name;

            users.find({}, function (err, data){
                if (!err) {
                    username = data[0].userName;
                    console.log('send email - ', 'addonId: ', addonId, 'email:', '', 'account: ', account, 'username:', username);
                    //for sending emails in node please use i.e. RGBboy/express-mailer
                }
            });
        }
    });
};

var exports = function() {

    var initialize = function () {
        router.get('/addons', controller.addons.get);
        router.put('/addons/:id', controller.addons.put);
        return router;
    };

    var controller = {
        addons: {
            get: function (req, res) {
                addons.find({}, function (err, data){
                    if (!err) {
                        res.json(data);
                    }
                });
            },
            put: function (req, res) {
                if (req.body.checked && req.body.type === 'premium') {
                    sendEmail(req.params.id);
                    req.body.type = 'requested';
                }

                //temporary code to reset premium status
                if (!req.body.checked && req.body.type === 'requested') {
                    req.body.type = 'premium';
                }

                addons.updateById(req.params.id, req.body, function () {
                    addons.findById(req.params.id, function (err, data) {
                        if (!err) {
                            res.json(data);
                        }
                    });
                });
            },
        }
    };

    return initialize();
};

module.exports = exports;
