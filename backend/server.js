/**
 * Created by elf on 11/09/15.
 */

var express = require('express'),
    app = express(),
    routes = require('./routes.js'),
    bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

routes.setRoutes(app);

app.use(require('connect-livereload')());

app.use(express.static(__dirname+'/public', { maxAge: 1 }));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/index.html', { maxAge: 1 });
});

app.listen(3001, function() {
    console.log('Server up: http://localhost:3001');
});