var addonModel = require('../models/addon.js');

var Model = Backbone.Collection.extend({
  model: addonModel,
  url: 'api/addons'
});

module.exports = Model;