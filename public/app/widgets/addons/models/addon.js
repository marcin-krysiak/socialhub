var Model = Backbone.Model.extend({
  idAttribute: '_id'
});

module.exports = Model;